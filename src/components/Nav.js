import React from "react";
import { Link } from "react-router-dom"

export default class Nav extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "/about": true,
            "/signup": true,
            "/": false
        }
    }




    render() {
       
        return (<div className="divLink">

            {this.state["/about"] && (<Link to="/about" className="link" onClick={() => {

                this.setState({
                    "/about": false,
                    "/": true,
                    "/signup": true
                })
            }}>About</Link>)}
            {this.state["/"] && (<Link to="/" className="link" onClick={() => {
                this.setState({
                    "/about": true,
                    "/": false,
                    "/signup": true
                })

            }}>Home</Link>)}
            {this.state["/signup"] && (<Link to="/signup" className="link" onClick={() => {
                this.setState({
                    "/about": true,
                    "/": true,
                    "/signup": false
                })

            }}>Signup</Link>)}
        </div>)
    }
}