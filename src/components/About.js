import React from "react";


class About extends React.Component {

    componentDidMount() {
        document.title = "About"
    }

    render() {
        return (
            <>
                <div className="otherdiv">
                    <h1>About us</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices congue erat, eu rhoncus massa ornare sed. Pellentesque sodales augue eget magna finibus, ut viverra erat volutpat. Aenean bibendum lacus nisl, ac molestie sem pellentesque a. Praesent sed venenatis ipsum. Duis quis ligula bibendum, iaculis neque in, porttitor dolor. Fusce ac rutrum libero, et facilisis enim.</p>
                </div>

                <div className="Whatwedo">
                    <h1>What we do</h1>
                    <div className="whatwechild">
                        <div>
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices congue erat, eu rhoncus massa ornare sed. Pellentesque sodales augue eget magna finibus, ut viverra erat volutpat. Aenean bibendum lacus nisl, ac molestie sem pellentesque a.</p>
                        </div>
                        <div>
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices congue erat, eu rhoncus massa ornare sed. Pellentesque sodales augue eget magna finibus, ut viverra erat volutpat. Aenean bibendum lacus nisl, ac molestie sem pellentesque a.</p>
                        </div>
                        <div>
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices congue erat, eu rhoncus massa ornare sed. Pellentesque sodales augue eget magna finibus, ut viverra erat volutpat. Aenean bibendum lacus nisl, ac molestie sem pellentesque a.</p>
                        </div>
                    </div>
                </div>
                <footer>
                    <p>Feel free to contact us at xyz@ipsum.com</p>
                </footer>
            </>

        )
    }
}

export default About