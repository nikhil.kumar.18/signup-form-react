import React from "react";

class Error extends React.Component {
    render() {
        return (<label className="label"><code className="error">{this.props.error}</code></label>)
    }
}

export default Error