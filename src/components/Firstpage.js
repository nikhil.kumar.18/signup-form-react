import React from "react";
import img1 from './img/img1.jpeg'
import img2 from './img/img2.jpg'
import img3 from './img/img3.jpg'
import img4 from './img/img4.jpg'


export default class First extends React.Component {

    componentDidMount() {
        document.title = "Welcome"
    }

    render() {

        return (
            <>
                <div className="firstpage">
                    <div className="otherdiv firstpagediv">
                        <h1>Welcome to my App</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et lectus quis quam fermentum cursus. Integer viverra ut ante at elementum. Cras facilisis commodo eros, at rhoncus felis. Cras lobortis elit tempor orci rutrum posuere. Cras eros elit, accumsan ac ipsum quis, euismod posuere dui. Nulla eleifend ligula nec leo luctus, egestas consectetur risus vulputate. </p>

                    </div>
                    <figure>
                        <img className="firstpageimg" src={img4} alt="image not found" title="Lorem Ipsum" />
                        <figcaption>Lorem ipsum</figcaption>
                    </figure>
                    <div className="articles">

                        <div>
                            <img src={img1} alt="imagenotfound" />
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et lectus quis quam fermentum cursus.</p>
                        </div>
                        <div>
                            <img src={img2} alt="imagenotfound" />
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et lectus quis quam fermentum cursus.</p>
                        </div>
                        <div>
                            <img src={img3} alt="imagenotfound" />
                            <h3>Lorem Ipsum</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et lectus quis quam fermentum cursus.</p>
                        </div>
                    </div>

                </div>
                <footer>
                    <p>Feel free to contact us at xyz@ipsum.com</p>
                </footer>
            </>
        )
    }

}