import React from 'react';

class NotFound extends React.Component {
    componentDidMount() {
        document.title = "Not Found"
    }
    render() {
        return (<h1 style={{ textAlign: "center" }}>404 Not Found!</h1>)
    }
}

export default NotFound;